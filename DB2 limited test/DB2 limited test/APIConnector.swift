//
//  RatesAPIConnector.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 24/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation

class APIConnector {
    
    static let shared = APIConnector()
    
    func sendRequest(url:String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> ()){
        guard let url = URL(string: url) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                completionHandler(nil,response,error)
                return
            }
            completionHandler(dataResponse,response,nil)
        }
        task.resume()
    }
}
