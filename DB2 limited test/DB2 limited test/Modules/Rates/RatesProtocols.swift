//
//  RatesProtocols.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 23/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation

protocol RatesConfiguratorProtocol: class {
    func configure(with viewController: RatesViewController)
}

protocol RatesViewToPresenterProtocol: class {
    func getPrivatBankCellsNumber() -> Int
    func getNBUCellsNumber() -> Int
    func getPrivatBankCellData(for index: IndexPath) -> PrivatBankDataModel
    func getNBUCellData(for index: IndexPath) -> NBUDataModel
    func didSelectPrivatBankCell(with index: IndexPath)
    func didSelectNBUCell(with index: IndexPath)
    func configureView()
}

protocol RatesPresenterToViewProtocol: class {
    func updatePrivatBankTable()
    func updateNBUTable()
}

protocol RatesPresenterToInteractorProtocol: class {
    func updatePrivatBankRates()
    func updateNBURates()
}

protocol RatesInteractorToPresenterProtocol: class {
    func didUpdatePrivatBankRates(data: [PrivatBankDataModel])
    func didUpdateNBURates(data: [NBUDataModel])
}
