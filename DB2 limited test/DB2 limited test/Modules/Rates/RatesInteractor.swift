
//
//  RatesInteractor.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 23/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation

class RatesInteractor: RatesPresenterToInteractorProtocol {
    weak var presenter: RatesInteractorToPresenterProtocol!
    
    let NBU_API_URL = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    let PrivatBank_API_URL = "https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11"
    
    required init(with presenter: RatesInteractorToPresenterProtocol){
        self.presenter = presenter
    }
    
    func updatePrivatBankRates() {
        APIConnector.shared.sendRequest(url: PrivatBank_API_URL) { (data, response, error) in
            guard let dataO = data else { return }
            do{
                let parsedModels = try JSONDecoder().decode([PrivatBankDataModel].self, from: dataO)
                self.presenter.didUpdatePrivatBankRates(data: parsedModels)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func updateNBURates() {
        APIConnector.shared.sendRequest(url: NBU_API_URL) { (data, response, error) in
            guard let dataO = data else { return }
            do{
                var parsedModels = try JSONDecoder().decode([NBUDataModel].self, from: dataO)
                parsedModels.sort(by: { $0.currencyText! < $1.currencyText! })
                self.presenter.didUpdateNBURates(data: parsedModels)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
