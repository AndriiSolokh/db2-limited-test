//
//  PrivatBankDataModel.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 23/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation

struct PrivatBankDataModel: Decodable {
    let currencyName: String?
    let buyRate: String?
    let saleRate: String?
    
    enum CodingKeys: String, CodingKey {
        case currencyName = "ccy"
        case buyRate = "buy"
        case saleRate = "sale"
    }
}
