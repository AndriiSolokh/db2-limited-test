//
//  NBUDataModel.swift
//  DB2 limited test
//
//  Created by Andre Solokh on 23/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation

struct NBUDataModel: Decodable {
    let currencyText: String?
    let rate: Double?
    let currencyName: String?
    
    enum CodingKeys: String, CodingKey {
        case currencyText = "txt"
        case rate
        case currencyName = "cc"
    }
}
