//
//  ViewController.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 23/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import UIKit

class RatesViewController: UIViewController {
    
    let cellIdentifierPrivatBank = "PrivatBankCell"
    let cellIdentifierNBU = "NBUCell"
    
    @IBOutlet weak var tablePrivatBank: UITableView!
    @IBOutlet weak var tableNBU: UITableView!
    
    var presenter: RatesViewToPresenterProtocol!
    var tablePrivatBankDelegate: PrivatBankTableDelegate!
    var tableNBUDelegate: NBUTableDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let configurator = RatesConfigurator()
        configurator.configure(with: self)
        
        assignTableDelegates()
        presenter.configureView()
    }
    
    func assignTableDelegates() {
        tablePrivatBankDelegate = PrivatBankTableDelegate(with: self, cellIdentifier: cellIdentifierPrivatBank)
        tableNBUDelegate = NBUTableDelegate(with: self, cellIdentifier: cellIdentifierNBU)
        
        tablePrivatBank.dataSource = tablePrivatBankDelegate
        tablePrivatBank.delegate = tablePrivatBankDelegate
        
        tableNBU.dataSource = tableNBUDelegate
        tableNBU.delegate = tableNBUDelegate
    }
}

extension RatesViewController: RatesPresenterToViewProtocol {
    func updatePrivatBankTable() {
        DispatchQueue.main.async {
            self.tablePrivatBank.reloadData()
        }
    }
    
    func updateNBUTable() {
        DispatchQueue.main.async {
            self.tableNBU.reloadData()
        }
    }
}
