//
//  PrivatBankTableDelegate.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 24/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation
import UIKit

class PrivatBankTableDelegate: RatesTablesDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return view!.presenter.getPrivatBankCellsNumber()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataPrivatBank = view!.presenter.getPrivatBankCellData(for: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PrivatBankCell
        cell.fillCell(with: dataPrivatBank)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view?.presenter.didSelectPrivatBankCell(with: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 32
    }
}
