//
//  RatesTablesDelegate.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 24/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation
import UIKit

class RatesTablesDelegate: NSObject, UITableViewDelegate {
    weak var view: RatesViewController?
    let cellIdentifier:String
    required init(with viewController: RatesViewController, cellIdentifier: String) {
        self.view = viewController
        self.cellIdentifier = cellIdentifier
    }
}
