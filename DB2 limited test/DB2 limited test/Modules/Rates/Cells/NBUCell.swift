//
//  NBUCell.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 24/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation
import UIKit

class NBUCell: UITableViewCell {
    
    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    func fillCell(with data: NBUDataModel) {
        currencyNameLabel.text = data.currencyText
        priceLabel.text = String(format: "%.4f",data.rate!)
    }
    
}
