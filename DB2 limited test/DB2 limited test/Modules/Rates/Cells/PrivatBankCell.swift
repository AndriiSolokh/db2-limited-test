//
//  PrivatBankCell.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 24/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation
import UIKit

class PrivatBankCell: UITableViewCell {
    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var buyPriceLabel: UILabel!
    @IBOutlet weak var sellPriceLabel: UILabel!
    
    func fillCell(with data: PrivatBankDataModel){
        currencyNameLabel.text = data.currencyName
        buyPriceLabel.text = data.buyRate
        sellPriceLabel.text = data.saleRate
    }
}
