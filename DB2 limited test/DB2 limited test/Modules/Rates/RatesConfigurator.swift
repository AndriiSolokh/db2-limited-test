//
//  RatesConfigurator.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 23/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation

class RatesConfigurator: RatesConfiguratorProtocol {
    func configure(with viewController: RatesViewController) {
        let presenter = RatesPresenter(with: viewController)
        let interactor = RatesInteractor(with: presenter)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
    }
}
