//
//  RatesPresenter.swift
//  DB2 limited test
//
//  Created by Andrii Solokh on 23/01/2019.
//  Copyright © 2019 SOli project. All rights reserved.
//

import Foundation

class RatesPresenter: RatesViewToPresenterProtocol {
    weak var view: RatesViewController?
    var interactor: RatesPresenterToInteractorProtocol?
    
    var dataPrivatBank: [PrivatBankDataModel] = []
    var dataNBU: [NBUDataModel] = []
    
    required init(with viewController : RatesViewController){
        self.view = viewController
    }
    
    func getPrivatBankCellsNumber() -> Int {
        return dataPrivatBank.count
    }
    
    func getNBUCellsNumber() -> Int {
        return dataNBU.count
    }
    
    func getPrivatBankCellData(for index: IndexPath) -> PrivatBankDataModel {
        return dataPrivatBank[index.item]
    }
    
    func getNBUCellData(for index: IndexPath) -> NBUDataModel {
        return dataNBU[index.item]
    }
    
    func configureView() {
        interactor?.updateNBURates()
        interactor?.updatePrivatBankRates()
    }
    
    func didSelectPrivatBankCell(with index: IndexPath) {
        let selectedItem = index.item
        let currencyOfSelectedItem = dataPrivatBank[selectedItem].currencyName
        guard let indexOfSelectedItem = dataNBU.index(where: { (item) -> Bool in
            item.currencyName == currencyOfSelectedItem
        }) else { return }
        let indexScrollTo = IndexPath(item: indexOfSelectedItem, section: 0)
        view?.tableNBU.selectRow(at: indexScrollTo, animated: true, scrollPosition: .middle)
    }
    
    func didSelectNBUCell(with index: IndexPath) {
        let selectedItem = index.item
        let currencyOfSelectedItem = dataNBU[selectedItem].currencyName
        guard let indexOfSelectedItem = dataPrivatBank.index(where: { (item) -> Bool in
            item.currencyName == currencyOfSelectedItem
        }) else { return }
        let indexScrollTo = IndexPath(item: indexOfSelectedItem, section: 0)
        view?.tablePrivatBank.selectRow(at: indexScrollTo, animated: true, scrollPosition: .middle)
    }
}

extension RatesPresenter: RatesInteractorToPresenterProtocol {
    func didUpdatePrivatBankRates(data: [PrivatBankDataModel]) {
        dataPrivatBank = data
        view?.updatePrivatBankTable()
    }
    
    func didUpdateNBURates(data: [NBUDataModel]) {
        dataNBU = data
        view?.updateNBUTable()
    }
}
